$(function() {

// Слайдер
$('.top-slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    // autoplaySpeed: 5000,
    // fade: true,
    pauseOnHover: false,
    arrows: false,
});



// Модальное окно
	$('.modal-btn').click( function(event){ 
		event.preventDefault(); 
		$('.modal-wrap').css('display', 'flex').fadeIn(400, 
		 	function(){ 
				$('.modal') 
					.css('display', 'block') 
					.animate({opacity: 1}, 200); 
		});
	});
	
	$('.modal-close').click( function(){ 
		$('.modal').css('display', 'none')
			.animate({opacity: 0}, 200,  
				function(){ 
					$(this).css('display', 'none'); 
					$('.modal-wrap').fadeOut(400); 
				}
			);
	});

// Лайк продукта
$('.product-like').click(function() {
	$(this).toggleClass('active');
})



$('.header-city__curent').click(function() {
	$('.header-city__box').toggleClass("active");
});


$('.header-close').click(function() {
	$(this).parent().toggleClass("active");
});

$('.promocode-btn').click(function() {
	$('.promocode-box').toggleClass("active");
})




// Табы
$('.tabs-nav a').click(function() {
    var _id = $(this).attr('href');
    var _targetElement = $(this).parents('.tabs-wrapper').find('#' + _id);
    $('.tabs-nav a').removeClass('active');
    $(this).addClass('active');
    $('.tabs-item').removeClass('active');
    _targetElement.addClass('active');
    return false;
});


})